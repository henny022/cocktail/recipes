# Erdbeer Soda
fruchtig-milder Longdrink für den Sommer
alkoholfrei

## Zutaten
- 1 Kugel Erdbeereis
- 2cl Zitronensaft
- 2cl Erdbeersirup
- Sodawasser

## Zubereitung
- Eis, Zitronensaft und Sirup mit Eiswürfeln schütteln
- in großen Tumbler seihen
- mit Soda auffüllen und kurz umrühren
