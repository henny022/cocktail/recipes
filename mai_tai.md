# Mai Tai
fruchtig-milder Longdrink für die Party
alkoholisch

## Zutaten
- 4cl brauner Rum
- 2cl brauner Rum 75% Vol.-%
- 1cl Curaçao weiß
- 1cl Mandelsirup
- 2cl Zitronensaft
- 4cl Ananassaft
- 2cl Orangensaft

## Zubereitung
- großes Ballonglas halb mit Crushed Ice füllen
- Zutaten im Ballonglas verrühren
- Dekorieren mit einer Limettenscheibe und einem Minzezweig
- mit Trinkhalm servieren
