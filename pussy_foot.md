# Pussy Foot
fruchtig-herber Longdrink für die Party
alkoholfrei

## Zutaten
- 5cl Orangensaft
- 3cl Zitronensaft
- 3cl Ananassaft
- 1cl Grenadine

## Zubereitung
- im Shaker mit Eis schütteln
- im Longdrinkglas mit Orangenscheibe am Rand servieren
